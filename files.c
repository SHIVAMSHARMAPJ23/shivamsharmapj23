#include<stdio.h>
int main()
{
    char c;
    FILE *f;
    f=fopen("INPUT.txt","w");
    printf("Enter ctrl+D to exit\n");
    printf("Enter the data into the file:\n");
    while((c=getchar())!=EOF)
    {
        fputc(c,f);
    }
    fclose(f);
    f=fopen("INPUT.txt","r");
    printf("\nThe contents of the file are :\n");
    while((c=fgetc(f))!=EOF)
    {
        putchar(c);
    }
    fclose(f);
    return 0;
}